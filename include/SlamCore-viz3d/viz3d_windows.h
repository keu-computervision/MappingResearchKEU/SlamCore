#ifndef SLAMCORE_VIZ3D_WINDOWS_H
#define SLAMCORE_VIZ3D_WINDOWS_H

#ifdef SLAM_WITH_VIZ3D

#include <map>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>

#include <viz3d/vtk_window.h>
#include <SlamCore/types.h>

namespace slam {

    // A VTKWindow for rendering and controlling multiple PolyDatas by groups.
    class MultiPolyDataWindow : public viz3d::VTKWindow {
    public:
        using viz3d::VTKWindow::VTKWindow;

        // Registers a PolyData with a given group name and a group id
        void AddPolyData(std::string &&group_name, int id, vtkSmartPointer<vtkPolyData> poly_data);

        // Removes the group from the window
        void EraseGroup(const std::string &group_name);

        // Removes a poly data
        void RemovePolyData(const std::string &group_name, int id);

        // Draws the ImGui Content for the window
        void DrawImGuiWindowConfigurations() override;

        // Updates Group Information from the Poly Data
        void UpdateGroupInformation();

        // Applies a transform to all the actors in the group
        void ApplyTransform(const std::string &group_name, vtkSmartPointer<vtkTransform> transform);

        // Resets the group transform (applies identity to all the actors in the group)
        void ResetTransform(const std::string &group_name);

        // Returns all the groups names
        std::vector<std::string> GetAllGroups();

    protected:
        // Colors the actor's group
        virtual void GroupOptionsPopup(const std::string &group);


        std::map<std::string, std::map<int, vtkSmartPointer<vtkActor>>> actors_by_group_;

        struct MultiPolyDataWindowImGuiVars_ {
            bool open_window_options = false;
        } imgui_vars2_;

        struct GroupImGuiVars {
            std::set<std::string> field_names;
            std::string selected_field;
            float scalar_range[2] = {0., 1.f};
            int selected = 0;
            bool apply_to_new_actors = true;
            bool apply_transform_to_all_ = true;
            vtkSmartPointer<vtkTransform> transform = nullptr;
        };
        bool do_update_group_information_ = true;
        std::map<std::string, GroupImGuiVars> group_imgui_vars_;
        std::mutex group_vars_mutex_;
    };

}

#endif // SLAM_WITH_VIZ3D

#endif //SLAMCORE_VIZ3D_WINDOWS_H
