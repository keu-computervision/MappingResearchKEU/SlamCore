#ifndef SLAMCORE_ITERATOR_H
#define SLAMCORE_ITERATOR_H

#include <iterator>

#include "SlamCore/traits.h"
#include "SlamCore/conversion.h"
#include "SlamCore/experimental/iterator/base_iterator.h"

namespace slam {

    /*!
     * @brief   transform iterator maps an iterator to an iterator of a different type
     *          The iterator maps reference of the source type to a reference of the new type via a UnaryFunction
     */
    template<class _UnaryFuncT, class _SourceIteratorT, class _SourceValueT, class _ValueT>
    class transform_iterator :
            public __composition_iterator<transform_iterator<_UnaryFuncT, _SourceIteratorT, _SourceValueT, _ValueT>,
                    _SourceIteratorT, _SourceValueT> {
    private:
        _UnaryFuncT func;
        using __parent_t = __composition_iterator<transform_iterator<_UnaryFuncT, _SourceIteratorT, _SourceValueT, _ValueT>,
                _SourceIteratorT, _SourceValueT>;
        using __parent_t::source_it;
        typedef convert_pointer_t<_SourceIteratorT, _ValueT> __pointer_t;
        typedef std::iterator_traits<__pointer_t> __std_iterator_traits;
    public:
        typedef typename _SourceIteratorT::iterator_category iterator_category;
        typedef typename _SourceIteratorT::difference_type difference_type;
        typedef typename __std_iterator_traits::value_type value_type;
        typedef typename __std_iterator_traits::pointer pointer;
        typedef typename __std_iterator_traits::reference reference;

        transform_iterator() : __parent_t() {};

        transform_iterator(_SourceIteratorT const &it, _UnaryFuncT f) : func(f), __parent_t(it) {}

        explicit transform_iterator(_SourceIteratorT const &it) : __parent_t(it) {};

        inline reference operator*() const { return func(*source_it); }

        inline pointer operator->() const { return &func(*source_it); }

        template<typename ReferenceT = reference>
        inline std::enable_if_t<std::is_same_v<ReferenceT, reference> &&
                                std::is_same_v<iterator_category, std::random_access_iterator_tag>, ReferenceT>
        operator[](difference_type __n) const {
            static_assert(std::is_same_v<iterator_category, std::random_access_iterator_tag>);
            return func(source_it[__n]);
        }

        inline const pointer base() const { return &func(*source_it.base()); }

    };


    /*!
     * @brief   Convenient method to make a transform_iterator with template deduction, for a given Conversion
     */
    template<typename _ConversionT, typename _SourceIteratorT>
    slam::transform_iterator<_ConversionT,
            _SourceIteratorT,
            typename _SourceIteratorT::value_type,
            typename _ConversionT::value_type> make_transform(_SourceIteratorT it,
                                                              _ConversionT) {
        static_assert(std::is_same_v<typename _ConversionT::conversion_category, reference_conversion_tag>,
                      "A transform iterator can only be applied with a conversion mapping two references");
        return slam::transform_iterator<_ConversionT, _SourceIteratorT,
                typename _SourceIteratorT::value_type, typename _ConversionT::value_type>(it);
    };


} // namespace slam

#endif //SLAMCORE_ITERATOR_H
