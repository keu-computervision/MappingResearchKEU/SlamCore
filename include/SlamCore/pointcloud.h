#ifndef SlamCore_POINTCLOUD_H
#define SlamCore_POINTCLOUD_H

#include <list>

#include "SlamCore/data/buffer_collection.h"

namespace slam {

    class PointCloud;

    typedef std::shared_ptr<PointCloud> PointCloudPtr;

    /*!
     * @brief   A PointCloud is a structure representing a set of 3D points with a custom set of fields.
     *          Each point in the PointCloud share the same number of these fields.
     *
     * A PointCloud mostly offers `views` of the data. The actual data layout is managed by a `BufferCollection`
     * Fields can be inserted, and deleted, but each property must have the same number of items equals to the number of points.
     *
     * The data layout and schema can be changed (at the cost of some copy).
     *
     */
    class PointCloud {
    public:

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Constructors and static factories
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        PointCloud(BufferCollection &&collection, std::string &&xyz_element) : collection_(std::move(collection)) {
            CHECK(collection_.HasElement(xyz_element));
            xyz_element_ = xyz_element;
        };

        // Returns an empty point cloud with a default XYZ floating point vector in its item schema
        template<typename ScalarT>
        PointCloud static DefaultXYZ();

        // Returns a pointcloud wrapping the data contained in data
        template<typename ItemT, typename Alloc_ = std::allocator<ItemT>>
        PointCloud static WrapVector(std::vector<ItemT, Alloc_> &data, ItemSchema &&schema,
                                     const std::string &xyz_element);

        template<typename ItemT, typename Alloc_ = std::allocator<ItemT>>
        const PointCloud static WrapConstVector(const std::vector<ItemT, Alloc_> &data, ItemSchema &&schema,
                                                const std::string &xyz_element) {
            return const_cast<const PointCloud>(WrapVector(const_cast<std::vector<ItemT, Alloc_> &>(data),
                                                           std::move(schema),
                                                           xyz_element));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Geometry API
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Changes the XYZ element to point to another element in the buffer collection
        void ChangeXYZElement(const std::string &element_name);

        // Returns a ProxyView of Eigen::Vector3(d/f) to the XYZ element
        template<typename ScalarT>
        ProxyView<Eigen::Matrix<ScalarT, 3, 1>> XYZ();

        // Returns a const ProxyView of Eigen::Vector3(d/f) to the XYZ element
        template<typename ScalarT>
        const ProxyView<Eigen::Matrix<ScalarT, 3, 1>> XYZConst() const;

        // Returns the number of points in the point cloud
        size_t size() const;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Schema Infos & Management
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Returns a const reference to the underlying buffer collection
        const BufferCollection &GetCollection() const;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// View API: Get properties and elements as views
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // Returns a view of a given element in the point cloud
        // Note:    The type `DestT` must have the same size as the element
        template<typename DestT>
        View<DestT> ElementView(const std::string &element_name);

        // Returns a view of a given element in the point cloud
        // Note:    The type `DestT` must have the same size as the element
        template<typename DestT>
        const View<DestT> ElementView(const std::string &element_name) const;

        // Returns a view of a property in the point cloud
        template<typename DestT>
        View<DestT> PropertyView(const std::string &element_name, const std::string &property_name);

        // Returns a view of a property in the point cloud
        template<typename DestT>
        const View<DestT> PropertyView(const std::string &element_name, const std::string &property_name) const;

        // Returns a Proxy View of a property in the point cloud
        // Note:    It is only compatible with an element if all its properties have the same type
        template<typename DestT>
        ProxyView<DestT> ElementProxyView(const std::string &element_name);

        // Returns a Proxy View of a property in the point cloud
        // Note:    It is only compatible with an element if all its properties have the same type
        template<typename DestT>
        const ProxyView<DestT> ElementProxyView(const std::string &element_name) const;

        template<typename DestT>
        ProxyView<DestT> PropertyProxyView(const std::string &element_name, const std::string &property_name);

        template<typename DestT>
        const ProxyView<DestT>
        PropertyProxyView(const std::string &element_name, const std::string &property_name) const;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Property / Element management API
        ///
        /// Methods to manipulate / add / remove / rename elements and properties
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Removes the Element from the schema of the point cloud
        // @note: The corresponding item buffer will only be freed if its schema is empty
        void RemoveElement(const std::string &element_name);

        // Add a structured item to the point cloud (defined by its type and schema)
        // This will allocate the necessary space
        template<typename T>
        void AddItemVectorBuffer(slam::ItemSchema &&schema);

        // Add a structured item (containing multiple elements) to the point cloud
        // The point cloud will make a Deep Copy of the vector, by copying it to a vector buffer
        template<typename T, typename Alloc_ = std::allocator<T>>
        void AddItemVectorDeepCopy(const std::vector<T, Alloc_> &item_data,
                                   slam::ItemSchema &&schema);




        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Resizable PointCloud API
        ///
        /// All method will raise an exception if at least one buffer is not Resizable,
        /// In which case IsResizable() would return false.
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        bool IsResizable() const;

        void resize(size_t new_size);

        void reserve(size_t new_size);

        template<typename T>
        void PushBackElement(const std::string &element_name, const T &element);;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Shallow / Deep Copy API
        ///
        /// These methods provide shallow / deep copies of the current point cloud.
        /// Shallow copies can restrict the view, providing access to less fields.
        /// Deep copies can change the layout, in which case the memory is managed
        /// By the returned PointCloud (via VectorBuffer collections)
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        // Whether the point cloud manages its own memory
//        // ie every one of its ItemBuffer manages its own memory,
//        bool ManagesOwnMemory() const;
//
//        // Copy constructor makes a deep copy of the data
//        PointCloud(const PointCloud &pointcloud);
//
//        // Move constructor transfer ownership of the buffer collections resources
//        PointCloud(PointCloud &&pointcloud);
//
//        // Assignment operator builds a deep copy of the pointcloud
//        PointCloud &operator=(const PointCloud &pointcloud);
//
//        // Move Assignment operator transfer the buffer_collections resources
//        PointCloud &operator=(PointCloud &&pointcloud);
//
//        // Returns a deep copy of the point cloud, in which all the fields are contiguously
//        // Laid in memory, in a common Item
//        PointCloud TightFitDeepCopy() const;
//
//        // Returns a deep copy of the point cloud, in which all the selected elements
//        // Are laid contiguously in memory
//        PointCloud TightFitDeepCopy(const std::vector<std::string> &element_names) const;
//
//        // Returns a shallow copy of the point cloud.
//        // Its buffers will simply provides views of the source point cloud buffers.
//        // The Shallow copy is only allowed on Shared pointer
//        static std::shared_ptr<PointCloud> ShallowCopy(std::shared_ptr<PointCloud> cloud_ptr);


    private:
        BufferCollection collection_;
        std::string xyz_element_;

        // Dependency management
        std::shared_ptr<PointCloud> parent_pointcloud_ = nullptr;
        std::list<PointCloud *> child_clouds_;
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// IMPLEMENTATIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename ScalarT>
    ProxyView<Eigen::Matrix<ScalarT, 3, 1>> PointCloud::XYZ() {
        static_assert(std::is_floating_point_v<ScalarT>);
        auto &xyz_element = collection_.GetElement(xyz_element_);
        CHECK(xyz_element.properties.size() == 3) << "Not a proper XYZ element defined in the schema" << std::endl;
        CHECK(xyz_element.properties[0].type == xyz_element.properties[1].type &&
              xyz_element.properties[0].type == xyz_element.properties[2].type)
                        << "Not a proper XYZ element defined in the schema" << std::endl;
        return collection_.template element_proxy<Eigen::Matrix<ScalarT, 3, 1>>(xyz_element_);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename ScalarT>
    PointCloud PointCloud::DefaultXYZ() {
        static_assert(std::is_floating_point_v<ScalarT>);
        using Vec3 = Eigen::Matrix<ScalarT, 3, 1>;
        std::vector<ItemBufferPtr> buffers(1);
        ItemBufferPtr ptr = std::make_unique<VectorBuffer>(
                ItemSchema::Builder(sizeof(Vec3))
                        .AddElement("vertex", 0).
                                template AddScalarProperty<ScalarT>("vertex", "X", 0).
                                template AddScalarProperty<ScalarT>("vertex", "Y", sizeof(ScalarT)).
                                template AddScalarProperty<ScalarT>("vertex", "Z", 2 * sizeof(ScalarT))
                        .Build(),
                sizeof(Vec3));
        buffers[0] = std::move(ptr);

        return PointCloud(BufferCollection(std::move(buffers)), "vertex");
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename T>
    void PointCloud::PushBackElement(const std::string &element_name, const T &element) {
        CHECK(IsResizable()) << "Cannot append an element to a non resizable buffer" << std::endl;
        CHECK(GetCollection().HasElement(element_name))
                        << "The element " << element_name << " does not exist in the collection";
        collection_.InsertItems(1);
        auto view = collection_.template element<T>(element_name);
        view.back() = element;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename T>
    void PointCloud::AddItemVectorBuffer(ItemSchema &&schema) {
        CHECK(schema.GetItemSize() == sizeof(T)) << "Incompatible data sizes" << std::endl;
        for (auto &element: schema.GetElementNames()) {
            CHECK(!collection_.HasElement(element))
                            << "The point cloud already has an element with the name: "
                            << element << std::endl;
        }
        auto buffer_ptr = std::make_unique<slam::VectorBuffer>(std::move(schema), sizeof(T));
        buffer_ptr->Resize(collection_.NumItemsPerBuffer());
        collection_.AddBuffer(std::move(buffer_ptr));
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename T, typename Alloc_>
    void PointCloud::AddItemVectorDeepCopy(const std::vector<T, Alloc_> &item_data, ItemSchema &&schema) {
        CHECK(item_data.size() == size()) << "Incompatible sizes (pointcloud:"
                                          << size() << "/ new element: " << item_data.size() << ")" << std::endl;
        CHECK(schema.GetItemSize() == sizeof(T)) << "Incompatible data sizes" << std::endl;
        for (auto &element: schema.GetElementNames()) {
            CHECK(!collection_.HasElement(element))
                            << "The point cloud already has an element with the name: "
                            << element << std::endl;
        }
        auto buffer_ptr = std::make_unique<slam::VectorBuffer>(std::move(schema), sizeof(T));
        buffer_ptr->Reserve(collection_.NumItemsPerBuffer());
        buffer_ptr->InsertItems(item_data.size(), reinterpret_cast<const char *>(&item_data[0]));
        collection_.AddBuffer(std::move(buffer_ptr));
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    View<DestT> PointCloud::ElementView(const std::string &element_name) {
        return collection_.element<DestT>(element_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    const View<DestT> PointCloud::ElementView(const std::string &element_name) const {
        return collection_.element<DestT>(element_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    template<typename DestT>
    View<DestT> PointCloud::PropertyView(const std::string &element_name, const std::string &property_name) {
        return collection_.property<DestT>(element_name, property_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    const View<DestT>
    PointCloud::PropertyView(const std::string &element_name, const std::string &property_name) const {
        return collection_.property<DestT>(element_name, property_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    ProxyView<DestT> PointCloud::ElementProxyView(const std::string &element_name) {
        return collection_.template element_proxy<DestT>(element_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    const ProxyView<DestT> PointCloud::ElementProxyView(const std::string &element_name) const {
        return collection_.template element_proxy<DestT>(element_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    ProxyView<DestT> PointCloud::PropertyProxyView(const std::string &element_name, const std::string &property_name) {
        return collection_.template property_proxy<DestT>(element_name, property_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename DestT>
    const ProxyView<DestT> PointCloud::PropertyProxyView(const std::string &element_name,
                                                         const std::string &property_name) const {
        return collection_.template property_proxy<DestT>(element_name, property_name);
    };

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename ScalarT>
    const ProxyView<Eigen::Matrix<ScalarT, 3, 1>> PointCloud::XYZConst() const {
        static_assert(std::is_floating_point_v<ScalarT>);
        auto &xyz_element = collection_.GetElement(xyz_element_);
        CHECK(xyz_element.properties.size() == 3) << "Not a proper XYZ element defined in the schema" << std::endl;
        CHECK(xyz_element.properties[0].type == xyz_element.properties[1].type &&
              xyz_element.properties[0].type == xyz_element.properties[2].type)
                        << "Not a proper XYZ element defined in the schema" << std::endl;
        return collection_.template element_proxy<Eigen::Matrix<ScalarT, 3, 1>>(xyz_element_);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    template<typename ItemT, typename Alloc_>
    PointCloud
    PointCloud::WrapVector(std::vector<ItemT, Alloc_> &data, ItemSchema &&schema, const std::string &xyz_element) {
        CHECK(schema.HasElement(xyz_element)) << "The schema does not contain the xyz element";
        const auto &elem_info = schema.GetElementInfo(xyz_element);
        CHECK(elem_info.properties.size() == 3 ||
              elem_info.properties.size() == 1 && elem_info.properties.front().dimension == 3);
        for (auto &property: elem_info.properties)
            CHECK(property.type == elem_info.properties.front().type);

        return PointCloud(
                slam::BufferCollection(slam::BufferWrapper::CreatePtr(data, std::move(schema))),
                std::string(xyz_element));
    }

} // namespace slam


#endif //SlamCore_POINTCLOUD_H
