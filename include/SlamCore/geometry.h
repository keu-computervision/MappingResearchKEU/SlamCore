#ifndef SlamCore_GEOMETRY_H
#define SlamCore_GEOMETRY_H

#include "SlamCore/types.h"

namespace slam {

    /**
     * Returns the Least-Square optimal transform between two weighted sets of points
     *
     * The transform returns is the optimal transform to be applied on `reference_points` to minimize
     * The Weighted LS distance between `target_points` and `reference_points`
     */
    SE3 OrthogonalProcrustes(const std::vector<Eigen::Vector3d> &reference_points,
                             const std::vector<Eigen::Vector3d> &target_points);

}

#endif //SlamCore_GEOMETRY_H
