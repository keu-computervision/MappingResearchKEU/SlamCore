#ifndef SlamCore_IO_H
#define SlamCore_IO_H

#include "SlamCore/types.h"

namespace slam {

    // A PointCloudSchema gives the necessary information to decode a point cloud from a PLY
    struct PointCloudSchema {

        enum TYPE {
            FLOAT, DOUBLE
        };

        typedef std::pair<std::string, std::string> pair_element_property;

        struct PointElement {
            std::string ply_element = "vertex";
            std::string x_property = "x";
            std::string y_property = "y";
            std::string z_property = "z";
        };

        PointElement raw_point_element;
        std::optional<pair_element_property> timestamp_element_and_property{};

        TYPE raw_point_write_type = FLOAT;
        TYPE timestamp_write_type = DOUBLE;

        inline static PointCloudSchema DefaultSchema() {
            PointCloudSchema schema{
                    {"vertex", "x", "y", "z"},
                    {{"vertex", "timestamp"}}};
            return schema;
        };
    };

    std::vector<uint8_t> ReadStreamAsByteArray(std::istream &stream);

    // Reads a PLY file from a Stream
    std::vector<slam::WPoint3D> ReadPLYFromStream(std::istream &stream, const PointCloudSchema &schema);

    // Reads a PLY file from disk
    std::vector<slam::WPoint3D> ReadPLYFromFile(const std::string &file_path, const PointCloudSchema &schema);

    // Writes a point cloud to a file
    void WritePLY(std::ostream &output_file, const std::vector<slam::WPoint3D> &points,
                  const PointCloudSchema &schema = PointCloudSchema::DefaultSchema());

    // Writes a point cloud to a file
    void WritePLY(const std::string &file, const std::vector<slam::WPoint3D> &points,
                  const PointCloudSchema &schema = PointCloudSchema::DefaultSchema());


    // Writes the vector of poses as a PLY binary file stream
    void SavePosesAsPLY(std::ostream &output_file,
                        const std::vector<Pose> &poses);

    // Writes the vector of poses as a binary PLY file on disk
    void SavePosesAsPLY(const std::string &file_path,
                        const std::vector<Pose> &poses);

    // Reads a vector of poses from a PLY binary file stream
    std::vector<slam::Pose> ReadPosesFromPLY(std::istream &input_file);

    // Reads a vector of poses from a PLY binary file on disk
    std::vector<slam::Pose> ReadPosesFromPLY(const std::string &input_file_path);

    // Saves Poses to disk, and returns whether the writing was successful
    bool SavePosesKITTIFormat(const std::string &file_path, const std::vector<Pose> &);

    // Loads Poses from disk. Raises a std::runtime_error if it fails to do so
    std::vector<Pose> LoadPosesKITTIFormat(const std::string &file_path);

} // namespace slam

#endif //SlamCore_IO_H
