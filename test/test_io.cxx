#include <fstream>
#include <gtest/gtest.h>

#include "SlamCore/types.h"
#include "SlamCore/generic_tools.h"
#include "SlamCore/io.h"

TEST(io, Read_WritePLYFile) {

    std::vector<slam::WPoint3D> points(10);
    for (auto &point: points) {
        point.RawPoint() = Eigen::Vector3d::Random();
        point.Timestamp() = (double) rand() / RAND_MAX;
    }

    std::stringstream ss_stream;
    slam::PointCloudSchema schema{
            {"vertex", "x", "y", "z"},
            {{"vertex", "timestamp"}},
            slam::PointCloudSchema::DOUBLE,
            slam::PointCloudSchema::DOUBLE};

    slam::WritePLY(ss_stream, points, schema);
    auto pc_copy = slam::ReadPLYFromStream(ss_stream, schema);


    ASSERT_EQ(pc_copy.size(), points.size());
    for (int i(0); i < points.size(); ++i) {
        double norm = (pc_copy[i].RawPointConst() - points[i].RawPoint()).norm();
        ASSERT_EQ(norm, 0.);
        ASSERT_EQ(pc_copy[i].Timestamp(), points[i].Timestamp());
    }
}

TEST(io, Read_Write_Poses_As_PLY) {
    std::vector<slam::Pose> poses(10);
    for (auto &pose: poses) {
        pose.pose.quat = Eigen::Quaterniond::UnitRandom();
        pose.pose.tr = Eigen::Vector3d::Random();
        pose.dest_frame_id = static_cast<slam::frame_id_t >(rand());
        pose.dest_timestamp = (double) rand() / RAND_MAX;
        pose.ref_frame_id = static_cast<slam::frame_id_t >(rand());
        pose.ref_timestamp = (double) rand() / RAND_MAX;
    }

    std::stringstream ss_stream;
    slam::SavePosesAsPLY(ss_stream, poses);
    auto copy_poses = slam::ReadPosesFromPLY(ss_stream);

    for (auto i(0); i < poses.size(); ++i) {
        auto &pose = poses[i];
        auto &copy = copy_poses[i];
        ASSERT_EQ(pose.dest_timestamp, copy.dest_timestamp);
        ASSERT_EQ(pose.dest_frame_id, copy.dest_frame_id);
        ASSERT_EQ(pose.ref_timestamp, copy.ref_timestamp);
        ASSERT_EQ(pose.ref_frame_id, copy.ref_frame_id);
        ASSERT_LE((pose.pose.Matrix() - copy.pose.Matrix()).cwiseAbs().maxCoeff(), 1.e-10);
    }


}

