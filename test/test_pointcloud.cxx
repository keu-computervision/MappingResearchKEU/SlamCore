#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <SlamCore/pointcloud.h>
#include <SlamCore/data/view.h>
#include "SlamCore/types.h"


TEST(PointCloud, building_the_pointcloud) {
    auto pc = slam::PointCloud::DefaultXYZ<double>();
    ASSERT_TRUE(pc.GetCollection().HasElement("vertex"));
    ASSERT_TRUE(pc.IsResizable());

    size_t n = 1000;
    pc.resize(n);

    ASSERT_EQ(pc.size(), n);

    // Both proxy views points to the same data on disk
    auto xyz_view = pc.XYZ<float>();
    auto xyz_view_d = pc.XYZConst<double>();

    ASSERT_EQ(xyz_view_d.size(), n);
    for (auto i(0); i < n; ++i) {
        Eigen::Vector3f point = xyz_view[i];
        ASSERT_EQ(point[0], 0.f);
        ASSERT_EQ(point[1], 0.f);
        ASSERT_EQ(point[2], 0.f);

        xyz_view[i] = Eigen::Vector3f(67.f, 42.f, 98.f);

        Eigen::Vector3d point_3d = xyz_view_d[i];
        ASSERT_EQ(point_3d[0], 67.);
        ASSERT_EQ(point_3d[1], 42.);
        ASSERT_EQ(point_3d[2], 98.);
    }

    // Push back to the point cloud
    pc.reserve(n * 2);
    for (auto i(0); i < n; ++i)
        pc.PushBackElement("vertex", Eigen::Vector3d(1., 2., 3.));
    ASSERT_EQ(xyz_view_d.size(), 2 * n);

    for (auto i(0); i < n; ++i) {
        Eigen::Vector3d point = xyz_view_d[i + n];
        ASSERT_EQ(point[0], 1.);
        ASSERT_EQ(point[1], 2.);
        ASSERT_EQ(point[2], 3.);
    }

    // Add a WrapperBuffer to the Point Cloud
    using rgb_t = std::array<unsigned char, 3>;
    std::vector<rgb_t> rgb(pc.size());
    for (auto &color: rgb) {
        color[0] = rand() % 255;
        color[1] = rand() % 255;
        color[2] = rand() % 255;
    }
    pc.AddItemVectorDeepCopy(rgb,
                             slam::ItemSchema::Builder(sizeof(rgb_t))
                                     .AddElement("rgb", 0)
                                     .AddScalarProperty<unsigned char>("rgb", "r", 0)
                                     .AddScalarProperty<unsigned char>("rgb", "g", 1)
                                     .AddScalarProperty<unsigned char>("rgb", "b", 2)
                                     .Build());

    auto elem_view = pc.ElementView<rgb_t>("rgb");
    auto elem_proxy_view = pc.ElementProxyView<std::array<int, 3>>("rgb");
    for (auto i(0); i < elem_view.size(); ++i) {
        auto &color = elem_view[i];
        auto &ref_color = rgb[i];
        std::array<int, 3> color_proxy = elem_proxy_view[i];
        for (auto k(0); k < 3; ++k) {
            ASSERT_EQ(color[k], ref_color[k]);
            ASSERT_EQ(color_proxy[k], ref_color[k]);
        }
    }


    std::vector<slam::WPoint3D> all_points(10);
    auto pointcloud = slam::PointCloud::WrapVector(all_points, slam::WPoint3D::DefaultSchema(), "raw_point");
    auto proxy_view = pointcloud.XYZ<double>();

    for (auto i(0); i < pointcloud.size(); ++i) {
        Eigen::Vector3d rand = Eigen::Vector3d::Random();
        proxy_view[i] = rand;
        ASSERT_EQ((all_points[i].raw_point.point - rand).norm(), 0.);
    }


}




