#include <gtest/gtest.h>

#include <thread>
#include <chrono>
#include <memory>

#include <SlamCore-viz3d/viz3d_utils.h>
#include <SlamCore-viz3d/viz3d_windows.h>

#include <viz3d/ui.h>
#include <viz3d/vtk_window.h>

#include <vtkPolyDataMapper.h>


TEST(viz3d_utils, test) {

    const auto kNumPoints = 10000;
    std::vector<slam::WPoint3D> points, points_bis;
    points.reserve(kNumPoints);
    points_bis.reserve(kNumPoints);

    for (auto k(0); k < kNumPoints; ++k) {
        slam::WPoint3D new_point;
        new_point.RawPoint() = Eigen::Vector3d::Random();
        new_point.RawPoint().z() = 0.;
        new_point.Timestamp() = std::abs(new_point.RawPoint().x());
        points.push_back(new_point);

        new_point.RawPoint() = Eigen::Vector3d::Random();
        new_point.RawPoint().x() = 0.;
        new_point.Timestamp() = std::pow(new_point.RawPoint().y(), 2) + new_point.RawPoint().z();
        points_bis.push_back(new_point);
    }

    auto poly_data = slam::polydata_from_points(points, false);
    auto poly_data_bis = slam::polydata_from_points(points_bis, false);

    std::vector<slam::SE3> poses(100);
    for (auto &pose: poses) {
        pose.quat = Eigen::Quaterniond::UnitRandom();
        pose.tr = Eigen::Vector3d::Random();
    }

    auto poly_data_poses = slam::polydata_from_poses(poses.begin(), poses.end(), 0.1);

    auto &gui = viz3d::GUI::Instance();
    auto window = std::make_shared<slam::MultiPolyDataWindow>("VTK Window");
    window->InitializeVTKContext();
    window->AddPolyData("Points", 0, poly_data);
    window->AddPolyData("Points_Bis", 0, poly_data_bis);
    window->AddPolyData("Poses", 0, poly_data_poses);

    gui.AddWindow(window);
    std::thread gui_thread{viz3d::GUI::LaunchMainLoop, "GUI"};

#ifndef VIZ3D_WAIT_FOR_CLOSE
    std::this_thread::sleep_for(std::chrono::duration<double, std::ratio<1, 1>>(2.0));
#endif
    gui.SignalClose();
    gui_thread.join();

    ASSERT_TRUE(true);
}


TEST(viz3d_utils, conversion) {

//    auto blue = slam::scalar_to_color(0., slam::JET);
//    auto red = slam::scalar_to_color(1., slam::JET);
//
//    ASSERT_EQ(blue.x(), 0.f);
//    ASSERT_EQ(blue.y(), 0.f);
//    ASSERT_GE(blue.z(), 0.1f);
//
//    ASSERT_GE(red.x(), 0.1f);
//    ASSERT_EQ(red.y(), 0.f);
//    ASSERT_EQ(red.z(), 0.f);
}



