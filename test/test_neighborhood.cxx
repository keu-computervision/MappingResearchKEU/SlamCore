#include <gtest/gtest.h>
#include "SlamCore/experimental/neighborhood.h"

TEST(test_neighborhood, eigen) {

    int n = 1000;
    std::vector<Eigen::Vector3d> points;
    points.resize(n);

    for (auto &point: points) {
        point = Eigen::Vector3d::Random();
    }
    slam::Neighborhood neighborhood(points);
    ASSERT_NO_THROW(neighborhood.ComputeNeighborhood(slam::ALL));
    slam::NearestNeighborSearchResult result(10, 0.1);
    neighborhood.SearchNearestNeighbors(Eigen::Vector3d::Random(), result.ResultSet());
    auto indices = result.Indices();
    CHECK(indices.size() >= 1);
}

TEST(test_neighborhood, world_point) {

    int n = 1000;
    std::vector<slam::WPoint3D> points;
    points.resize(n);
    for (auto &point: points)
        point.WorldPoint() = Eigen::Vector3d::Random();

    slam::WorldPointNeighborhood neighborhood(points);
    ASSERT_NO_THROW(neighborhood.ComputeNeighborhood(slam::ALL));
    slam::NearestNeighborSearchResult result(10, 0.1);
    neighborhood.SearchNearestNeighbors(Eigen::Vector3d::Random(), result.ResultSet());
    auto indices = result.Indices();
    CHECK(indices.size() >= 1);
}



