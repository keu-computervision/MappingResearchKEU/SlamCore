#! /bin/bash

src_dir=$(pwd)
build_dir=${src_dir}/cmake-build-release
superbuild_dir=$(cd ..; pwd)
install_dir=${superbuild_dir}/install

if [ ! -d "${build_dir}" ]
then
   mkdir "${build_dir}"
fi

check_status_code() {
   if [ $1 -ne 0 ]
   then
	echo "[SlamCore] Failure. Exiting."
	exit 1
   fi
}

cd "${build_dir}" || (echo "Failed to go in ${build_dir}" && exit)
cmake "${src_dir}" -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_INSTALL_DIR=${install_dir}
check_status_code $?

cmake --build . --config Release --target install
check_status_code $?

popd
