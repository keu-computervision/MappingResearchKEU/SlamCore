#ifdef SLAM_WITH_VIZ3D

#include "SlamCore-viz3d/viz3d_utils.h"

#include <colormap/colormap.hpp>
#include <SlamCore/generic_tools.h>

#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

namespace slam {

//    /* -------------------------------------------------------------------------------------------------------------- */
//    viz::ArrayV3f slam_to_viz3d_pc(const std::vector<slam::WPoint3D> &points, bool world_points) {
//        viz::ArrayV3f new_points(points.size());
//        std::transform(points.begin(), points.end(), new_points.begin(), [world_points](const auto &wpoint) {
//            return (world_points ? wpoint.world_point : wpoint.raw_point.point).template cast<float>();
//        });
//        return new_points;
//    }
//
//    namespace {
//
//        std::string GetColorMap(COLOR_SCHEME scheme) {
//            switch (scheme) {
//                case COLOR_SCHEME::MAGMA:
//                    return "magma";
//                case COLOR_SCHEME::JET:
//                    return "jet";
//                case COLOR_SCHEME::VIRIDIS:
//                    return "viridis";
//                default:
//                    LOG(INFO) << "The color scheme " << scheme << " does not exist" << std::endl;
//                    return "jet";
//            }
//        }
//    }
//
//    /* -------------------------------------------------------------------------------------------------------------- */
//    viz::ArrayV3f get_viz3d_color(const std::vector<double> &scalars, bool normalize, COLOR_SCHEME cmap) {
//        std::vector<double> copy;
//        viz::ArrayV3f viz_points(scalars.size());
//        if (normalize) {
//            copy = scalars;
//            double min_value = *std::min_element(scalars.begin(), scalars.end());
//            double max_value = *std::max_element(scalars.begin(), scalars.end());
//            double scalar;
//            for (auto &value: copy) {
//                scalar = (value - min_value) / (max_value - min_value);
//                value = scalar;
//            }
//        }
//
//        auto palette = colormap::palettes.at(GetColorMap(cmap)).rescale(0, 1);
//        for (auto i(0); i < scalars.size(); ++i) {
//            double s = normalize ? copy[i] : std::min(std::max(scalars[i], 0.), 1.);
//            colormap::rgb value = palette(s);
//            std::uint8_t *rgb_color_ptr = reinterpret_cast<std::uint8_t *>(&value);
//            Eigen::Vector3f rgb((float) rgb_color_ptr[0] / 255.0f,
//                                (float) rgb_color_ptr[1] / 255.0f, (float) rgb_color_ptr[2] / 255.0f);
//            viz_points[i] = rgb;
//        }
//
//        return viz_points;
//    }
//
//    /* -------------------------------------------------------------------------------------------------------------- */
//    viz::ArrayV3f get_field_color(const std::vector<slam::WPoint3D> &points, COLOR_SCHEME cmap, COLOR_FIELD cfield) {
//        std::vector<double> scalars(points.size());
//        double scalar;
//        for (auto i(0); i < points.size(); ++i) {
//            auto &point = points[i];
//            switch (cfield) {
//                case X:
//                    scalar = point.WorldPointConst().x();
//                    break;
//                case Y:
//                    scalar = point.WorldPointConst().y();
//                    break;
//                case Z:
//                    scalar = point.WorldPointConst().z();
//                    break;
//                case T:
//                default:
//                    scalar = point.TimestampConst();
//                    break;
//            }
//            scalars[i] = scalar;
//        }
//
//        return get_viz3d_color(scalars, true, cmap);
//    }
//
//    /* -------------------------------------------------------------------------------------------------------------- */
//    viz::ArrayM4f slam_to_viz3d_poses(const std::vector<slam::Pose> &poses) {
//        return transform_vector<viz::glMatrix4f,
//                viz::ArrayM4f::allocator_type>(poses,
//                                               [](const auto &pose) {
//                                                   Eigen::Matrix4f mat = pose.Matrix().template cast<float>();
//                                                   return mat;
//                                               });;
//    }
//
//    /* -------------------------------------------------------------------------------------------------------------- */
//    Eigen::Vector3f scalar_to_color(double value, COLOR_SCHEME cmap) {
//        CHECK(0. <= value && value <= 1.) << "The value must be in the range [0, 1.]" << std::endl;
//        const auto &palette = colormap::palettes.at(GetColorMap(cmap));
//        auto rgb = palette(value);
//        auto &_rgb = *reinterpret_cast<const std::array<unsigned char, 3> *> (&rgb);
//        return Eigen::Vector3f(float(_rgb[0]) / 255.f, float(_rgb[1]) / 255.f, float(_rgb[2]) / 255.f);
//    }


    /* -------------------------------------------------------------------------------------------------------------- */
    vtkSmartPointer<vtkPolyData> polydata_from_points(const std::vector<slam::WPoint3D> &points, bool world_points) {
        auto poly_data = vtkSmartPointer<vtkPolyData>::New();
        const auto num_points = points.size();

        // Setup the array of points
        auto vtk_points = vtkSmartPointer<vtkPoints>::New();
        vtk_points->SetDataTypeToFloat();
        vtk_points->Allocate(points.size());
        vtk_points->SetNumberOfPoints(points.size());
        vtk_points->GetData()->SetName("Points_XYZ");
        poly_data->SetPoints(vtk_points.GetPointer());

        // Assign for each cell vertex indices
        auto cells = vtkSmartPointer<vtkIdTypeArray>::New();
        cells->SetNumberOfValues(num_points * 2);
        vtkIdType *ids = cells->GetPointer(0);
        for (vtkIdType i = 0; i < num_points; ++i) {
            ids[i * 2] = 1; // num points in the cell = 1
            ids[i * 2 + 1] = i; // pid
        }
        vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();
        cellArray->SetCells(num_points, cells.GetPointer());
        poly_data->SetVerts(cellArray);

        {
            // Double Field for timestamps
            std::map<std::string, vtkSmartPointer<vtkFloatArray>> fields{
                    {"T", vtkSmartPointer<vtkFloatArray>::New()},
                    {"X", vtkSmartPointer<vtkFloatArray>::New()},
                    {"Y", vtkSmartPointer<vtkFloatArray>::New()},
                    {"Z", vtkSmartPointer<vtkFloatArray>::New()},
            };
            for (auto &pair: fields) {
                pair.second->Allocate(num_points);
                pair.second->SetNumberOfTuples(num_points);
                pair.second->SetName(pair.first.c_str());
                poly_data->GetPointData()->AddArray(pair.second);
            }

            for (auto idx(0); idx < points.size(); ++idx) {
                auto &wpoint = points[idx];
                Eigen::Vector3f xyz;
                if (world_points)
                    xyz = wpoint.WorldPointConst().cast<float>();
                else
                    xyz = wpoint.RawPointConst().cast<float>();
                vtk_points->SetPoint(idx, xyz.data());

                fields["T"].GetPointer()->SetValue(idx, (float) wpoint.TimestampConst());
                fields["X"].GetPointer()->SetValue(idx, (float) xyz.x());
                fields["Y"].GetPointer()->SetValue(idx, (float) xyz.y());
                fields["Z"].GetPointer()->SetValue(idx, (float) xyz.z());
//            dynamic_cast<vtkIntArray *>(fields["FID"])->SetValue(idx, (int) wpoint.index_frame);
            }
        }

        poly_data->GetPointData()->SetActiveScalars("T");
        return poly_data;
    }

} // namespace

#endif
