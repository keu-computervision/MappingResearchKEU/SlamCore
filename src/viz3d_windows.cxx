#include "SlamCore-viz3d/viz3d_windows.h"
#include <algorithm>

#include <vtkPolyDataMapper.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkLinearTransform.h>
#include <vtkTransform.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkCellData.h>

namespace slam {

    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::AddPolyData(std::string &&group_name,
                                          int id, vtkSmartPointer<vtkPolyData> poly_data) {

        auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputData(poly_data);
        vtkNew<vtkActor> actor;
        actor->SetMapper(mapper);
        actors_by_group_[group_name][id] = actor;
        AddActor(actor);
        do_update_group_information_ = true;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::RemovePolyData(const std::string &group_name, int id) {
        if (actors_by_group_.find(group_name) != actors_by_group_.end()) {
            auto &group = actors_by_group_[group_name];
            if (group.find(id) != group.end()) {
                auto actor = group[id];
                RemoveActor(actor);
                group.erase(id);
                do_update_group_information_ = true;
            }
        }
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::EraseGroup(const std::string &group_name) {
        if (actors_by_group_.find(group_name) != actors_by_group_.end()) {
            actors_by_group_.erase(group_name);
            group_imgui_vars_.erase(group_name);
            do_update_group_information_ = true;
        }
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::DrawImGuiWindowConfigurations() {
        ImGui::PushID("context_menu");
        ImGui::MenuItem("Window Options", NULL, &imgui_vars2_.open_window_options);
        ImGui::PopID();

        if (do_update_group_information_) {
            UpdateGroupInformation();
            do_update_group_information_ = false;
        }

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(4.0, 4.0));

        std::string winname = window_name_ + " Options";

        if (imgui_vars2_.open_window_options && ImGui::Begin(winname.c_str())) {
            ImGui::Text("Window Parameters:             ");
            ImGui::Separator();

            if (ImGui::CollapsingHeader("Window Configuration")) {
                BackgroundPopup();
                RenderingPopup();
            }

            if (ImGui::CollapsingHeader("Group Configurations")) {
                int idx(0);
                for (auto &group: actors_by_group_) {
                    GroupOptionsPopup(group.first);
                    if (idx != actors_by_group_.size() - 1) {
                        ImGui::SameLine();
                    }
                    idx++;
                }
            }

            ImGui::End();
        }
        ImGui::PopStyleVar();
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::GroupOptionsPopup(const std::string &group) {

        ImGui::PushID(group.c_str());
        if (ImGui::Button(group.c_str()))
            ImGui::OpenPopup("color_group");

        if (ImGui::BeginPopup("color_group")) {
            ImGui::Text("Color Scale Options:");

            auto &group_options = group_imgui_vars_[group];
            const char *combo_preview_value = nullptr;
            if (!group_options.field_names.empty()
                && group_options.selected < group_options.field_names.size()
                && group_options.selected >= 0) {
                auto it = group_options.field_names.begin();
                std::advance(it, group_options.selected);
                combo_preview_value = (*it).c_str();
            } else
                combo_preview_value = "";

            ImGui::Checkbox("Apply to new Actors", &group_options.apply_to_new_actors);
            if (ImGui::BeginCombo("Field Selection", combo_preview_value)) {
                auto it = group_options.field_names.begin();
                for (int i(0); i < group_options.field_names.size(); i++) {
                    const bool is_selected = (group_options.selected == i);
                    if (ImGui::Selectable((*it).c_str(), is_selected)) {
                        group_options.selected = i;
                        group_options.selected_field = *it;
                    }
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();
                    it++;
                }
                ImGui::EndCombo();
            }


            ImVec2 button_size = ImVec2(
                    (ImGui::GetContentRegionAvail().x - ImGui::GetStyle().FramePadding.x) * 0.5f,
                    2 * ImGui::GetFontSize());
            ImGui::InputFloat2("Scalar Range", group_options.scalar_range);
            if (ImGui::Button("Set to Min-Max", ImVec2(button_size.x * 2.f, button_size.y))) {
                float min_max[2] = {std::numeric_limits<float>::max(), std::numeric_limits<float>::min()};
                double actor_min_max[2];

                for (auto &actor: actors_by_group_[group]) {
                    auto scalars = actor.second->GetMapper()->GetInput()->GetPointData()->GetScalars();
                    if (scalars) {
                        scalars->GetRange(actor_min_max);
                        if (actor_min_max[0] < min_max[0])
                            min_max[0] = float(actor_min_max[0]);
                        if (actor_min_max[1] > min_max[0])
                            min_max[1] = float(actor_min_max[1]);
                    }
                }
                if (min_max[0] != std::numeric_limits<float>::max() &&
                    min_max[1] != std::numeric_limits<float>::min()) {
                    group_options.scalar_range[0] = min_max[0];
                    group_options.scalar_range[1] = min_max[1];
                }

                for (auto &actor: actors_by_group_[group]) {
                    actor.second->GetMapper()->SetScalarRange(group_options.scalar_range[0],
                                                              group_options.scalar_range[1]);
                }
            }


            static float scale = 1.f;
            static float xyz[3] = {0.f, 0.f, 0.f};
            if (ImGui::TreeNode("Transform Options:")) {
                ImGui::InputFloat("Model Scale", &scale, 0.1f, 1.f);
                ImGui::InputFloat3("Model Translation (XYZ)", xyz);
                ImGui::TreePop();
            }

            ImGui::Separator();
            if (ImGui::Button("Apply", button_size)) {
                auto new_transform = vtkSmartPointer<vtkTransform>::New();
                new_transform->Scale(scale, scale, scale);
                new_transform->Translate(xyz);
                ApplyTransform(group, new_transform);
                for (auto &actor: actors_by_group_[group]) {
                    auto *input = actor.second->GetMapper()->GetInput();
                    input->GetPointData()->SetActiveScalars(group_options.selected_field.c_str());
                    input->GetCellData()->SetActiveScalars(group_options.selected_field.c_str());
                }
            }

            ImGui::SameLine();
            if (ImGui::Button("Close", button_size))
                ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
        }
        ImGui::PopID();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::UpdateGroupInformation() {
        std::lock_guard lock{actors_management_mutex_};
        for (auto &group: actors_by_group_) {
            auto &group_name = group.first;
            auto &actors = group.second;
            if (group_imgui_vars_.find(group_name) == group_imgui_vars_.end())
                group_imgui_vars_[group_name] = {};
            auto &group_params = group_imgui_vars_[group_name];
            if (group_params.apply_transform_to_all_) {
                if (group_params.transform) {
                    for (auto &actor: actors) {
                        actor.second->SetUserTransform(group_params.transform);
                    }
                }
                group_params.apply_transform_to_all_ = false;
            }

            std::set<std::string> scalar_fields{""};
            for (auto &actor: actors) {

                // Find Point Data Fields
                auto *mapper = actor.second->GetMapper();
                auto *point_data = actor.second->GetMapper()->GetInput()->GetPointData();
                auto *cell_data = actor.second->GetMapper()->GetInput()->GetCellData();

                auto insert_array_names = [&](auto &data) {
                    auto num_arrays = data->GetNumberOfArrays();
                    for (auto array_id(0); array_id < num_arrays; array_id++) {
                        auto array_name = std::string(data->GetArrayName(array_id));
                        if (scalar_fields.find(array_name) == scalar_fields.end())
                            scalar_fields.insert(array_name);
                    }
                };
                insert_array_names(point_data);
                insert_array_names(cell_data);

                std::swap(group_params.field_names, scalar_fields);
                if (group_params.apply_to_new_actors) {
                    cell_data->SetActiveScalars(group_params.selected_field.c_str());
                    point_data->SetActiveScalars(group_params.selected_field.c_str());
                    mapper->SetScalarRange(group_params.scalar_range[0], group_params.scalar_range[1]);
                    if (!group_params.transform)
                        group_params.transform = vtkSmartPointer<vtkTransform>::New();
                    actor.second->SetUserTransform(group_params.transform);
                }
            }
        }
        do_update_group_information_ = false;
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::ApplyTransform(const std::string &group_name, vtkSmartPointer<vtkTransform> transform) {
        if (group_imgui_vars_.find(group_name) != group_imgui_vars_.end()) {
            std::lock_guard lock{actors_management_mutex_};
            group_imgui_vars_[group_name].transform = transform;
            group_imgui_vars_[group_name].apply_transform_to_all_ = true;
            do_update_group_information_ = true;
        }
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void MultiPolyDataWindow::ResetTransform(const std::string &group_name) {
        ApplyTransform(group_name, vtkSmartPointer<vtkTransform>::New());
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<std::string> MultiPolyDataWindow::GetAllGroups() {
        std::vector<std::string> group_names;
        for (auto &group: actors_by_group_)
            group_names.push_back(group.first);
        return group_names;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
}

