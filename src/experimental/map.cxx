#include "SlamCore/experimental/map.h"

namespace slam {

    /* -------------------------------------------------------------------------------------------------------------- */
    IMap::~IMap() = default;

} // namespace slam