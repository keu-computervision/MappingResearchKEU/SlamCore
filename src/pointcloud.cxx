#include "SlamCore/pointcloud.h"

namespace slam {

    /* -------------------------------------------------------------------------------------------------------------- */
    bool PointCloud::IsResizable() const {
        return collection_.IsResizable();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void PointCloud::resize(size_t new_size) {
        collection_.Resize(new_size);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void PointCloud::reserve(size_t new_size) {
        collection_.Reserve(new_size);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    const BufferCollection &PointCloud::GetCollection() const {
        return collection_;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    size_t PointCloud::size() const {
        return collection_.NumItemsPerBuffer();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void PointCloud::ChangeXYZElement(const std::string &element_name) {
        CHECK(collection_.HasElement(element_name))
                        << "The buffer collection of the point cloud does not have the element "
                        << element_name << std::endl;
        auto &element = collection_.GetElement(element_name);
        CHECK(element.properties.size() == 3 ||
              (element.properties.size() == 1) && element.properties[0].dimension == 3)
                        << "The element with name " << element_name << " is not a valid element" << std::endl;
        xyz_element_ = element_name;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void PointCloud::RemoveElement(const std::string &element_name) {
        CHECK(element_name != xyz_element_) << "Cannot delete the XYZ points element of a point cloud" << std::endl;
        if (!GetCollection().HasElement(element_name))
            return;
        collection_.RemoveElement(element_name);
    }

    /* -------------------------------------------------------------------------------------------------------------- */

}

