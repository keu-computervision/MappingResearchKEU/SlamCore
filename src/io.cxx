#include <fstream>
#include <iomanip>
#include <tinyply/tinyply.h>

#include "SlamCore/utils.h"
#include "SlamCore/io.h"
#include "SlamCore/generic_tools.h"

namespace slam {

    namespace {

        // Reads Data from a buffer into a buffer of Points
        template<typename Elem_Type_, bool WithTimestamp = false>
        void ReadPointsFromBuffer(slam::WPoint3D *points_buffer,
                                  const unsigned char *buffer,
                                  size_t count,
                                  const int offset_of_vertex =
                                  offsetof(slam::WPoint3D, raw_point) +
                                  offsetof(slam::Point3D, point)) {

            typedef std::array<Elem_Type_, WithTimestamp ? 4 : 3> element;
            auto *elem_buffer = reinterpret_cast<const element *>(buffer);

            double *vertex_ptr = nullptr;
            double *timestamp_ptr = nullptr;

            element copied_element;
            for (int idx(0); idx < count; idx++) {
                auto &new_point = points_buffer[idx];
                {
                    auto *_ptr = reinterpret_cast<unsigned char *>(&new_point);
                    vertex_ptr = reinterpret_cast<double *>(_ptr + offset_of_vertex);
                    if (WithTimestamp)
                        timestamp_ptr = reinterpret_cast<double *>(_ptr +
                                                                   offsetof(slam::WPoint3D, raw_point.timestamp));
                }
                copied_element = elem_buffer[idx];
                for (int dim(0); dim < 3; dim++)
                    vertex_ptr[dim] = static_cast<double>(copied_element[dim]);
                if (WithTimestamp)
                    *timestamp_ptr = static_cast<double>(copied_element[3]);
            }
        }
    }

    namespace {

        struct memory_buffer : public std::streambuf {
            char *p_start{nullptr};
            char *p_end{nullptr};
            size_t size;

            memory_buffer(char const *first_elem, size_t size)
                    : p_start(const_cast<char *>(first_elem)), p_end(p_start + size), size(size) {
                setg(p_start, p_start, p_end);
            }

            pos_type seekoff(off_type off, std::ios_base::seekdir dir, std::ios_base::openmode which) override {
                if (dir == std::ios_base::cur) gbump(static_cast<int>(off));
                else setg(p_start, (dir == std::ios_base::beg ? p_start : p_end) + off, p_end);
                return gptr() - p_start;
            }

            pos_type seekpos(pos_type pos, std::ios_base::openmode which) override {
                return seekoff(pos, std::ios_base::beg, which);
            }
        };

        struct memory_stream : virtual memory_buffer, public std::istream {
            memory_stream(char const *first_elem, size_t size)
                    : memory_buffer(first_elem, size), std::istream(static_cast<std::streambuf *>(this)) {}
        };

    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<WPoint3D> ReadPLYFromStream(std::istream &stream,
                                            const PointCloudSchema &schema) {

        const auto &bytes = ReadStreamAsByteArray(stream);
        auto _memory_stream = std::make_unique<memory_stream>((char *) bytes.data(), bytes.size());

        std::vector<WPoint3D> points;
        tinyply::PlyFile file;
        try {
            file.parse_header(*_memory_stream);

            auto &raw_point_elem = schema.raw_point_element;
            std::vector<std::string> vertex_properties{raw_point_elem.x_property,
                                                       raw_point_elem.y_property,
                                                       raw_point_elem.z_property};
            bool timestamp_in_vertex = false;
            bool with_timestamp = schema.timestamp_element_and_property.has_value();
            if (with_timestamp && schema.timestamp_element_and_property->first == raw_point_elem.ply_element) {
                vertex_properties.push_back(schema.timestamp_element_and_property->second);
                timestamp_in_vertex = true;
            }
            auto main_element_ptr = file.request_properties_from_element(raw_point_elem.ply_element,
                                                                         vertex_properties);

            std::shared_ptr<tinyply::PlyData> timestamp_ptr = nullptr;
            std::shared_ptr<tinyply::PlyData> world_point_ptr = nullptr;

            file.read(*_memory_stream);

            const auto kCount = main_element_ptr->count;
            const auto *buffer = main_element_ptr->buffer.get();
            points.resize(kCount);
            if (main_element_ptr->t == tinyply::Type::FLOAT64) {
                if (timestamp_in_vertex)
                    ReadPointsFromBuffer<double, true>(&points[0], buffer, kCount);
                else
                    ReadPointsFromBuffer<double, false>(&points[0], buffer, kCount);
            }
            if (main_element_ptr->t == tinyply::Type::FLOAT32) {
                if (timestamp_in_vertex)
                    ReadPointsFromBuffer<float, true>(&points[0], buffer, kCount);
                else
                    ReadPointsFromBuffer<float, false>(&points[0], buffer, kCount);
            }

        } catch (...) {
            std::cout << "Failed to read a ply file." << std::endl;
            for (auto &info: file.get_info())
                std::cout << info << std::endl;
            throw;
        }

        return points;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<slam::WPoint3D> ReadPLYFromFile(const std::string &file_path, const PointCloudSchema &schema) {
#if WITH_STD_FILESYSTEM
        CHECK(fs::exists(file_path) &&
              fs::is_regular_file(file_path)) << "The file " << file_path
                                              << " does not exist" << std::endl;
#endif
        std::ifstream binary_file(file_path.c_str(), std::ios::binary);
        CHECK(binary_file.is_open()) << "Could not open file located at " << file_path << std::endl;

        auto points = ReadPLYFromStream(binary_file, schema);
        binary_file.close();
        return points;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    tinyply::Type SlamTypeToTinyPLY(PointCloudSchema::TYPE type) {
        switch (type) {
            case PointCloudSchema::FLOAT:
                return tinyply::Type::FLOAT32;
            default:
                return tinyply::Type::FLOAT64;
        }
    }

    namespace {
        template<typename SourceT, typename DestT, int DIM, int offset>
        void CopyPropertyToBuffer(const std::vector<WPoint3D> &points,
                                  std::vector<uint8_t> &buffer) {
            CHECK(buffer.size() == points.size() * sizeof(DestT) * DIM) << "Mismatch sizes";
            auto elements = transform_vector<std::array<DestT, DIM>>(points, [](auto &point) {
                std::array<DestT, DIM> data;
                auto *ptr = reinterpret_cast<const SourceT *>(reinterpret_cast<const uint8_t *>(&point) + offset);
                for (int i(0); i < DIM; ++i) {
                    data[i] = static_cast<DestT>(ptr[i]);
                }
                return data;
            });
            auto data_ptr = reinterpret_cast<const uint8_t *>(&elements[0]);
            std::copy(data_ptr, data_ptr + buffer.size(), buffer.begin());
        }

    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void WritePLY(std::ostream &output_file,
                  const std::vector<slam::WPoint3D> &points,
                  const PointCloudSchema &schema) {

        tinyply::PlyFile file;

        const int kDataSize = schema.raw_point_write_type == PointCloudSchema::FLOAT ? sizeof(float) : sizeof(double);
        std::vector<uint8_t> data(points.size() * kDataSize * 3);
        std::unique_ptr<std::vector<uint8_t>> data_timestamps = nullptr;

        if (schema.raw_point_write_type == PointCloudSchema::FLOAT) {
            CopyPropertyToBuffer<double, float, 3, offsetof(slam::WPoint3D, raw_point)>(points, data);
        } else
            CopyPropertyToBuffer<double, double, 3, offsetof(slam::WPoint3D, raw_point)>(points, data);


        file.add_properties_to_element(schema.raw_point_element.ply_element, {
                                               schema.raw_point_element.x_property,
                                               schema.raw_point_element.y_property,
                                               schema.raw_point_element.z_property
                                       },
                                       SlamTypeToTinyPLY(schema.raw_point_write_type),
                                       points.size(),
                                       &data[0],
                                       tinyply::Type::INVALID, 0);

        if (schema.timestamp_element_and_property.has_value()) {
            auto &pair = schema.timestamp_element_and_property;
            if (pair->first == schema.raw_point_element.ply_element)
                CHECK(schema.raw_point_write_type == schema.raw_point_write_type);

            const int kTDataSize = schema.timestamp_write_type == PointCloudSchema::FLOAT ?
                                   sizeof(float) : sizeof(double);
            data_timestamps = std::make_unique<std::vector<std::uint8_t>>(points.size() * kTDataSize);


            if (schema.timestamp_write_type == PointCloudSchema::FLOAT)
                CopyPropertyToBuffer<double, float, 1,
                        offsetof(slam::WPoint3D, raw_point.timestamp)>(points, *data_timestamps);
            else
                CopyPropertyToBuffer<double, double, 1,
                        offsetof(slam::WPoint3D, raw_point.timestamp)>(points, *data_timestamps);

            file.add_properties_to_element(pair->first, {pair->second},
                                           SlamTypeToTinyPLY(schema.timestamp_write_type), points.size(),
                                           &(*data_timestamps)[0], tinyply::Type::INVALID, 0);
        }

        file.write(output_file, true);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void WritePLY(const std::string &file, const std::vector<slam::WPoint3D> &points, const PointCloudSchema &schema) {
        std::ofstream output_stream(file, std::ios::out | std::ios::binary);
        WritePLY(output_stream, points, std::move(schema));
        output_stream.close();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<uint8_t> ReadStreamAsByteArray(std::istream &stream) {
        std::vector<uint8_t> fileBufferBytes;
        stream.seekg(0, std::ios::end);
        size_t sizeBytes = stream.tellg();
        stream.seekg(0, std::ios::beg);
        fileBufferBytes.resize(sizeBytes);
        CHECK(stream.read((char *) fileBufferBytes.data(), sizeBytes));
        return fileBufferBytes;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<Pose> LoadPosesKITTIFormat(const std::string &file_path) {
        std::vector<Pose> poses;
        std::ifstream pFile(file_path);
        if (pFile.is_open()) {
            size_t iter(0);
            Pose pose;
            while (!pFile.eof()) {
                std::string line;
                std::getline(pFile, line);
                if (line.empty()) continue;
                std::stringstream ss(line);

                pose.dest_frame_id = iter;
                pose.dest_timestamp = static_cast<double>(iter) * 0.1;
                Eigen::Matrix4d P = Eigen::Matrix4d::Identity();
                ss >> P(0, 0) >> P(0, 1) >> P(0, 2) >> P(0, 3) >> P(1, 0) >> P(1, 1) >> P(1, 2) >> P(1, 3) >> P(2, 0)
                   >> P(2, 1) >> P(2, 2) >> P(2, 3);

                pose.pose.quat = Eigen::Quaterniond(P.block<3, 3>(0, 0));
                pose.pose.tr = Eigen::Vector3d(P.block<3, 1>(0, 3));
                poses.push_back(pose);
            }
            pFile.close();
        } else {
            std::cout << "Unable to open file" << std::endl;
        }
        return poses;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    bool SavePosesKITTIFormat(const std::string &file_path, const std::vector<Pose> &trajectory) {
        auto parent_path = fs::path(file_path).parent_path();
        if (!exists(parent_path))
            fs::create_directories(parent_path);
        std::ofstream pFile(file_path);
        if (pFile.is_open()) {
            pFile << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
            Eigen::Matrix4d pose;
            Eigen::Matrix3d R;
            Eigen::Vector3d t;
            for (auto &_pose: trajectory) {
                pose = _pose.Matrix();
                R = pose.block<3, 3>(0, 0);
                t = pose.block<3, 1>(0, 3);

                pFile << R(0, 0) << " " << R(0, 1) << " " << R(0, 2) << " " << t(0)
                      << " " << R(1, 0) << " " << R(1, 1) << " " << R(1, 2) << " "
                      << t(1) << " " << R(2, 0) << " " << R(2, 1) << " " << R(2, 2)
                      << " " << t(2) << std::endl;
            }
            pFile.close();

            std::cout << "Saved Poses to " << file_path << std::endl;
            return true;
        }
        std::cout << "Could not open file " << file_path << std::endl;
        return false;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    namespace {
        struct Float64PosesData {
            double qs[4], tr[3], ref_t, dest_t;
        };

        typedef std::pair<int, int> pair_fid;
    }

    void SavePosesAsPLY(std::ostream &output_file, const std::vector<Pose> &poses) {
        tinyply::PlyFile file;


        std::vector<Float64PosesData> se3_poses = transform_vector<Float64PosesData>(poses, [](const auto &pose) {
            auto &quat = pose.pose.quat;
            auto &tr = pose.pose.tr;
            return Float64PosesData{{quat.coeffs()[0], quat.coeffs()[1], quat.coeffs()[2], quat.coeffs()[3]},
                                    {tr[0], tr[1], tr[2]}, pose.ref_timestamp, pose.dest_timestamp};
        });

        std::vector<pair_fid> frame_ids = transform_vector<pair_fid>(poses, [](const auto &pose) {
            return std::make_pair(static_cast<int>(pose.ref_frame_id), static_cast<int>(pose.dest_frame_id));
        });


        file.add_properties_to_element("pose", {
                                               "qx", "qy", "qz", "qw",
                                               "x", "y", "z", "ref_t", "dest_t"
                                       },
                                       tinyply::Type::FLOAT64,
                                       poses.size(),
                                       reinterpret_cast<uint8_t *>(&se3_poses[0].qs[0]),
                                       tinyply::Type::INVALID, 0);
        file.add_properties_to_element("pose", {
                                               "ref_fid", "dest_fid"
                                       }, tinyply::Type::INT32, poses.size(),
                                       reinterpret_cast<uint8_t *>(&frame_ids[0].first),
                                       tinyply::Type::INVALID, 0);
        file.write(output_file, true);
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    void SavePosesAsPLY(const std::string &output_file_path, const std::vector<Pose> &poses) {
        std::ofstream output_stream(output_file_path, std::ios::out | std::ios::binary);
        SavePosesAsPLY(output_stream, poses);
        output_stream.close();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<slam::Pose> ReadPosesFromPLY(std::istream &stream) {
        std::vector<slam::Pose> poses;

        const auto &bytes = ReadStreamAsByteArray(stream);
        auto _memory_stream = std::make_unique<memory_stream>((char *) bytes.data(), bytes.size());

        tinyply::PlyFile file;
        try {
            file.parse_header(*_memory_stream);
            auto quat_tr_timestamps = file.request_properties_from_element("pose",
                                                                           {
                                                                                   "qx", "qy", "qz", "qw", "x", "y",
                                                                                   "z",
                                                                                   "ref_t", "dest_t"
                                                                           });
            auto fids = file.request_properties_from_element("pose",
                                                             {"ref_fid", "dest_fid"});

            std::shared_ptr<tinyply::PlyData> timestamp_ptr = nullptr;
            std::shared_ptr<tinyply::PlyData> world_point_ptr = nullptr;

            file.read(*_memory_stream);

            const auto kCount = quat_tr_timestamps->count;
            const auto *quat_tr_ts_raw_buffer = quat_tr_timestamps->buffer.get();
            const auto *fids_raw_buffer = fids->buffer.get();
            CHECK(quat_tr_timestamps->t == tinyply::Type::FLOAT64)
                            << "Invalid types in poses file (expected Float64)" << std::endl;
            poses.resize(kCount);

            auto *quat_tr_ts_buffer = reinterpret_cast<const Float64PosesData *>(quat_tr_ts_raw_buffer);
            auto *fids_buffer = reinterpret_cast<const pair_fid *>(fids_raw_buffer);

            Pose new_pose;
            for (int idx(0); idx < kCount; idx++) {
                const auto &elem = quat_tr_ts_buffer[idx];
                const auto &_fids = fids_buffer[idx];

                for (int i(0); i < 4; ++i)
                    new_pose.pose.quat.coeffs()[i] = elem.qs[i];
                for (int i(0); i < 3; ++i)
                    new_pose.pose.tr[i] = elem.tr[i];
                new_pose.dest_timestamp = elem.dest_t;
                new_pose.ref_timestamp = elem.ref_t;
                new_pose.ref_frame_id = static_cast<slam::frame_id_t>(_fids.first);
                new_pose.dest_frame_id = static_cast<slam::frame_id_t>(_fids.second);
                poses[idx] = new_pose;
            }
        } catch (...) {
            std::cout << "Failed to read a ply file." << std::endl;
            for (auto &info: file.get_info())
                std::cout << info << std::endl;
            throw;
        }

        return poses;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<slam::Pose> ReadPosesFromPLY(const std::string &file_path) {
        CHECK(fs::exists(file_path) &&
              fs::is_regular_file(file_path)) << "The file " << file_path
                                              << " does not exist" << std::endl;
        std::ifstream input_file(file_path, std::ios::binary);
        CHECK(input_file.is_open()) << "Could not open file on disk at location: " << file_path << std::endl;
        auto poses = ReadPosesFromPLY(input_file);
        input_file.close();
        return poses;
    }


} // namespace
