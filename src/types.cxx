#include "SlamCore/types.h"

#include <Eigen/Dense>
#include <glog/logging.h>

namespace slam {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// IMPLEMENTATIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* -------------------------------------------------------------------------------------------------------------- */
    Voxel Voxel::Coordinates(const Eigen::Vector3d &point, double voxel_size) {
        Voxel voxel;
        voxel.x = int(point.x() / voxel_size);
        voxel.y = int(point.y() / voxel_size);
        voxel.z = int(point.z() / voxel_size);

        return voxel;
    }


    /* -------------------------------------------------------------------------------------------------------------- */
    Eigen::Vector3d
    ContinousTransform(const Eigen::Vector3d &point, const Eigen::Quaterniond &begin_quat,
                       const Eigen::Vector3d &begin_tr,
                       const Eigen::Quaterniond &end_quat, const Eigen::Vector3d &end_tr, double relative_timestamp) {
        Eigen::Quaterniond slerp = begin_quat.slerp(relative_timestamp, end_quat);
        return slerp * point + (1 - relative_timestamp) * begin_tr + relative_timestamp * end_tr;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    std::vector<double> TimeStamps(const std::vector<slam::WPoint3D> &points) {
        std::vector<double> data(points.size());
        for (auto i(0); i < points.size(); ++i) {
            data[i] = points[i].raw_point.timestamp;
        }
        return data;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    ItemSchema WPoint3D::DefaultSchema() {
        return slam::ItemSchema::Builder(sizeof(WPoint3D))
                .AddElement("xyzt", offsetof(WPoint3D, raw_point.point))
                .AddElement("raw_point", offsetof(WPoint3D, raw_point.point))
                .AddElement("world_point", offsetof(WPoint3D, world_point))
                .AddElement("properties", 0)
                .AddScalarProperty<double>("xyzt", "xyz", 0, 3)
                .AddScalarProperty<double>("xyzt", "t", offsetof(Point3D, timestamp))
                .AddScalarProperty<double>("raw_point", "x", 0)
                .AddScalarProperty<double>("raw_point", "y", sizeof(double))
                .AddScalarProperty<double>("raw_point", "z", 2 * sizeof(double))
                .AddScalarProperty<double>("world_point", "x", 0)
                .AddScalarProperty<double>("world_point", "y", sizeof(double))
                .AddScalarProperty<double>("world_point", "z", 2 * sizeof(double))
                .AddScalarProperty<double>("properties", "raw_xyz", offsetof(WPoint3D, raw_point.point), 3)
                .AddScalarProperty<double>("properties", "t", offsetof(WPoint3D, raw_point.timestamp), 1)
                .AddScalarProperty<double>("properties", "world_xyz", offsetof(WPoint3D, world_point), 3)
                .AddScalarProperty<frame_id_t>("properties", "index_frame", offsetof(WPoint3D, index_frame), 1)
                .Build();
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    ItemSchema Point3D::DefaultSchema() {
        return slam::ItemSchema::Builder(sizeof(Point3D))
                .AddElement("xyzt", offsetof(Point3D, point))
                .AddElement("properties", 0)
                .AddScalarProperty<double>("xyzt", "xyz", 0, 3)
                .AddScalarProperty<double>("xyzt", "t", offsetof(Point3D, timestamp))
                .AddScalarProperty<double>("properties", "x", 0)
                .AddScalarProperty<double>("properties", "y", sizeof(double))
                .AddScalarProperty<double>("properties", "z", 2 * sizeof(double))
                .AddScalarProperty<double>("properties", "t", offsetof(Point3D, timestamp))
                .Build();
    }

} // namespace slam

