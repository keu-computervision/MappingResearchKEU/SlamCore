/**
 * A program which computes the metrics given two PLY trajectory paths
 */
#include <fstream>
#include <thread>

#include <tclap/CmdLine.h>
#include <yaml-cpp/yaml.h>

#include <SlamCore/io.h>
#include <SlamCore/eval.h>
#include <SlamCore/generic_tools.h>

#ifdef SLAM_WITH_VIZ3D

#include <viz3d/ui.h>

#endif

struct Options {

    std::string gt_trajectory_filepath;

    std::vector<std::string> trajectories_filepaths;

    std::optional<std::string> output_filepath;

    double segment_length = 100.0;

    bool visualize_segments = false;

    double max_timestamp = -1.0;

};


Options ReadArguments(int argc, char **argv) {
    Options options;

    try {
        TCLAP::CmdLine cmd("Computes and outputs trajectory metrics between two trajectories saved on disk",
                           ' ', "v0.9");
        TCLAP::ValueArg<std::string> gt_trajectory_path("g", "gt_path",
                                                        "Path to the ground truth PLY trajectory file on disk",
                                                        true, "", "string");
        TCLAP::MultiArg<std::string> trajectories_to_compare("t", "trajectory",
                                                             "Path to a PLY trajectory to compare against the ground truth",
                                                             true, "string");
        TCLAP::ValueArg<std::string> output_file_path("o", "output_path",
                                                      "Path to the output YAML containing the trajectory metrics",
                                                      false, "", "string");
        TCLAP::ValueArg<double> segment_length("s", "segment_length",
                                               "The length of the segment in kitti metrics",
                                               false, 1000.0, "double");
        TCLAP::ValueArg<bool> visualize_arg("v", "visualize", "Whether to visualize the different trajectories",
                                            false, true, "bool");

        TCLAP::ValueArg<double> max_timestamp_arg("m", "max_timestamp", "The maximum timestamp of poses considered",
                                                  false, -1.0, "float");

        cmd.add(gt_trajectory_path);
        cmd.add(trajectories_to_compare);
        cmd.add(output_file_path);
        cmd.add(segment_length);
        cmd.add(visualize_arg);
        cmd.add(max_timestamp_arg);
        cmd.parse(argc, argv);

        options.segment_length = segment_length.getValue();
        options.gt_trajectory_filepath = gt_trajectory_path.getValue();
        options.trajectories_filepaths = trajectories_to_compare.getValue();
        options.visualize_segments = visualize_arg.getValue();
        options.max_timestamp = max_timestamp_arg.getValue();
        if (!output_file_path.getValue().empty())
            options.output_filepath.emplace(output_file_path.getValue());

    } catch (...) {
        throw;
    }

    return options;
};


int main(int argc, char **argv) {
    auto options = ReadArguments(argc, argv);

    auto cut_poses_by_timestamps = [&options](auto &poses) {
        if (options.max_timestamp > 0.) {
            auto it = std::find_if(poses.begin(), poses.end(), [&options](auto &_pose) {
                return _pose.dest_timestamp >= options.max_timestamp;
            });
            auto num_frames = std::distance(poses.begin(), it);
            poses.resize(num_frames);
        }
    };

    slam::LinearContinuousTrajectory gt_trajectory;
    {
        auto gt_poses = slam::ReadPosesFromPLY(options.gt_trajectory_filepath);
        cut_poses_by_timestamps(gt_poses);
        gt_trajectory = slam::LinearContinuousTrajectory::Create(std::move(gt_poses));
    }
    LOG(INFO) << "Loaded ground truth poses with " << gt_trajectory.Poses().size() << " poses." << std::endl;

    std::vector<std::vector<slam::Pose>> trajectories(options.trajectories_filepaths.size());
    std::transform(options.trajectories_filepaths.begin(),
                   options.trajectories_filepaths.end(),
                   trajectories.begin(),
                   [&cut_poses_by_timestamps](const auto &path) {
                       auto poses = slam::ReadPosesFromPLY(path);
                       cut_poses_by_timestamps(poses);
                       return poses;
                   });

    struct MetricsStruct {
        std::string filepath;
        slam::kitti::seq_errors kitti_errors;
        slam::metrics_t slam_metrics;
        std::vector<slam::Pose> gt_poses;
        const std::vector<slam::Pose> *estimated_poses;
        YAML::Node node;
    };


    // Compute all metrics
    std::map<size_t, MetricsStruct> map_id_metrics;
    for (auto idx(0); idx < trajectories.size(); ++idx) {
        const auto &trajectory = trajectories[idx];
        auto interpolated_gt_poses = slam::transform_vector<slam::Pose>(trajectory,
                                                                        [&gt_trajectory](const auto &pose) {
                                                                            return gt_trajectory.InterpolatePose(
                                                                                    pose.dest_timestamp,
                                                                                    true);
                                                                        });
        LOG(INFO) << "Loaded estimated poses with "
                  << interpolated_gt_poses.size()
                  << " poses." << std::endl;


        auto pose_to_matrix = [](const auto &pose) {
            return pose.Matrix();
        };

        auto saved_matrices = slam::transform_vector<Eigen::Matrix4d,
                Eigen::aligned_allocator<Eigen::Matrix4d>>(trajectory, pose_to_matrix);
        auto gt_matrices = slam::transform_vector<Eigen::Matrix4d,
                Eigen::aligned_allocator<Eigen::Matrix4d>>(interpolated_gt_poses, pose_to_matrix);


        MetricsStruct metrics;
        metrics.estimated_poses = &trajectory;
        metrics.filepath = options.trajectories_filepaths[idx];

        // Compute Trajectory Metrics
        metrics.slam_metrics = slam::ComputeTrajectoryMetrics(interpolated_gt_poses, trajectory);
        metrics.kitti_errors = slam::kitti::EvaluatePoses(gt_matrices,
                                                          saved_matrices,
                                                          std::vector<double>{options.segment_length});

        // Build the YAML NODES
        metrics.node["kitti_node"] = slam::kitti::GenerateMetricYAMLNode({{"KITTI_METRICS",
                                                                           std::move(metrics.kitti_errors)}});
        metrics.node["TRAJECTORY_METRICS"] = slam::GenerateTrajectoryMetricsYAMLNode(metrics.slam_metrics);
        metrics.node["SEGMENT_LEN"] = options.segment_length;
        map_id_metrics.emplace(idx, std::move(metrics));
    }

    std::stringstream description;
    // Sort the poses their different metrics
    description << "File paths and their indices: " << std::endl << std::endl;
    for (auto &pair: map_id_metrics) {
        description << pair.first << ":\t" << pair.second.filepath << std::endl;
    }
    description << std::endl;
    // Sort metrics by Mean RPE
    std::vector<size_t> indices(options.trajectories_filepaths.size());
    std::iota(indices.begin(), indices.end(), 0);

    std::sort(indices.begin(), indices.end(), [&](size_t lhs, size_t rhs) {
        return map_id_metrics[lhs].kitti_errors.mean_rpe < map_id_metrics[rhs].kitti_errors.mean_rpe;
    });

    description << "/**********************************************************/" << std::endl;
    description << "Mean RPE metrics sorted: " << std::endl << std::endl;
    for (auto idx: indices)
        description << idx << ":\t" << map_id_metrics[idx].kitti_errors.mean_rpe << std::endl;
    description << std::endl;

    // Sort metrics by ATE (after correction)
    std::sort(indices.begin(), indices.end(), [&](size_t lhs, size_t rhs) {
        return map_id_metrics[lhs].slam_metrics.mean_ate < map_id_metrics[rhs].slam_metrics.mean_ate;
    });
    description << "/**********************************************************/" << std::endl;
    description << "Mean ATE metrics sorted: " << std::endl << std::endl;

    for (auto idx: indices) {
        description << idx << ":\t" << map_id_metrics[idx].slam_metrics.mean_ate << std::endl;
    }
    description << std::endl;

    // Print the console
    std::cout << description.str();

    if (options.output_filepath) {
        YAML::Node root_node;
        root_node["DESCRIPTION"] = description.str();

        for (auto pair: map_id_metrics)
            root_node[std::to_string(pair.first)] = pair.second.node;

        std::ofstream output_file(options.output_filepath.value());
        CHECK(output_file.is_open()) << "The output file is not opened" << std::endl;
        output_file << root_node << std::endl;
        output_file.close();
    }

#ifdef SLAM_WITH_VIZ3D
    // Visualize segments
//    if (options.visualize_segments) {
//        std::thread gui_thread{viz3d::GUI::LaunchMainLoop, "GUI Thread"};
//
//        auto idx = indices[0];
//        auto &poses = *map_id_metrics[idx].estimated_poses;
//
//        auto gt_model_ptr = std::make_shared<viz::PointCloudModel>();
//        auto best_trajectory_ptr = std::make_shared<viz::PointCloudModel>();
//        auto best_corrected_model_ptr = std::make_shared<viz::PointCloudModel>();
//        auto best_segments_model_ptr = std::make_shared<viz::PointCloudModel>();
//
//        auto &gt_model_data = gt_model_ptr->ModelData();
//        auto &best_model_data = best_trajectory_ptr->ModelData();
//        auto &best_corrected_model_data = best_corrected_model_ptr->ModelData();
//        auto &best_segments_model_data = best_segments_model_ptr->ModelData();
//
//        auto &instance = viz::ExplorationEngine::Instance();
//        auto pose_to_loc = [](const auto &pose) { return pose.pose.tr.template cast<float>(); };
//
//        gt_model_data.default_color = Eigen::Vector3f(0.f, 1.f, 0.f);
//        gt_model_data.point_size = 3;
//        gt_model_data.xyz = slam::transform_vector<Eigen::Vector3f,
//                Eigen::aligned_allocator<Eigen::Vector3f>>(gt_trajectory.Poses(), pose_to_loc);
//
//        best_model_data.default_color = Eigen::Vector3f(1.f, 0.f, 0.f);
//        best_model_data.point_size = 3;
//        best_model_data.xyz = slam::transform_vector<Eigen::Vector3f,
//                Eigen::aligned_allocator<Eigen::Vector3f>>(poses, pose_to_loc);
//        auto &xyz = best_model_data.xyz;
//        auto &point0 = xyz[0];
//
//
//        auto rigid_transform = map_id_metrics[idx].slam_metrics.rigid_transform;
//        if (rigid_transform) {
//            slam::SE3 Tr = rigid_transform->Inverse();
//            best_corrected_model_data.default_color = Eigen::Vector3f(0.f, 0.f, 1.f);
//            best_corrected_model_data.point_size = 3;
//            best_corrected_model_data.xyz = slam::transform_vector<Eigen::Vector3f,
//                    Eigen::aligned_allocator<Eigen::Vector3f>>(best_model_data.xyz,
//                                                               [&Tr](const auto &point) {
//                                                                   Eigen::Vector3d tr = point.template cast<double>();
//                                                                   Eigen::Vector3f tr_f = (Tr *
//                                                                                           tr).template cast<float>();
//                                                                   return tr_f;
//                                                               });
//            best_segments_model_data.default_color = Eigen::Vector3f(1.f, 1.f, 0.f);
//            best_segments_model_data.point_size = 5;
//            best_segments_model_data.xyz = best_model_data.xyz;
//            for (auto &segment: map_id_metrics[idx].slam_metrics.trajectory_segments) {
//                auto rigid_transform = segment.rigid_transform;
//                if (rigid_transform) {
//                    auto T = rigid_transform->Inverse();
//                    for (auto p_idx(segment.start_idx); p_idx < segment.end_idx; p_idx++) {
//                        Eigen::Vector3d pt = best_segments_model_data.xyz[p_idx].cast<double>();
//                        best_segments_model_data.xyz[p_idx] = (T * pt).cast<float>();
//                    }
//                }
//            }
//        }
//
//        instance.AddModel(0, gt_model_ptr);
//        instance.AddModel(1, best_trajectory_ptr);
//        if (rigid_transform) {
//            instance.AddModel(2, best_corrected_model_ptr);
//            instance.AddModel(3, best_segments_model_ptr);
//        }
//
//        gui_thread.join();
//    }
#endif

    return 0;
}