#include <iostream>
#include <tclap/CmdLine.h>
#include <SlamCore/io.h>
#include <SlamCore/trajectory.h>
#include <SlamCore/experimental/synthetic.h>
#include <SlamCore/utils.h>


#define _USE_MATH_DEFINES

#include <cmath>
#include <csignal>

/* ------------------------------------------------------------------------------------------------------------------ */
struct Options {

    std::string source_trajectory_path;

    std::string output_trajectory_path;

    double rnoise_rotation = 0.5; // The covariance of the gaussian noise on relative rotation (in degrees)

    double rnoise_translation = 0.5; // The covariance of the gaussian noise on relative translation (in meters)

};

/* ------------------------------------------------------------------------------------------------------------------ */
Options read_arguments(int argc, char **argv) {
    Options options;

    try {
        TCLAP::CmdLine cmd("Applies noise to a given trajectory (on the relative poses)",
                           ' ', "v0.9");
        TCLAP::ValueArg<std::string> source_arg("s", "source_path",
                                                "Path to the source PLY trajectory file on disk",
                                                true, "", "string");
        TCLAP::ValueArg<std::string> output_arg("o", "output_path",
                                                "The output path of the noisy trajectory",
                                                true, "", "string");
        TCLAP::ValueArg<double> rnoise_rotation_arg("r", "rotation_noise",
                                                    "The noise to apply on relative rotations (in degrees)",
                                                    false, 0.1, "double");
        TCLAP::ValueArg<double> rnoise_translation_arg("t", "translation_noise",
                                                       "The noise to apply on relative translations (in meters)",
                                                       false, 0.5, "double");
        cmd.add(source_arg);
        cmd.add(output_arg);
        cmd.add(rnoise_rotation_arg);
        cmd.add(rnoise_translation_arg);
        cmd.parse(argc, argv);

        options.output_trajectory_path = output_arg.getValue();
        options.source_trajectory_path = source_arg.getValue();
        options.rnoise_rotation = rnoise_rotation_arg.getValue();
        options.rnoise_translation = rnoise_translation_arg.getValue();

    } catch (...) {
        throw;
    }

    return options;
}

/* ------------------------------------------------------------------------------------------------------------------ */
int main(int argc, char **argv) {
    slam::setup_signal_handler(argc, argv);
    auto options = read_arguments(argc, argv);
    auto source_poses = slam::ReadPosesFromPLY(options.source_trajectory_path);
    std::vector<slam::Pose> corrected_trajectory; // Keeps only one pose per frame
    corrected_trajectory.reserve(source_poses.size());
    std::set<slam::frame_id_t> frame_ids;
    for (auto &pose: source_poses) {
        if (frame_ids.find(pose.dest_frame_id) == frame_ids.end()) {
            frame_ids.emplace(pose.dest_frame_id);
            corrected_trajectory.push_back(pose);
        }
    }

    // Compute the relative poses
    auto trajectory = slam::LinearContinuousTrajectory::Create(std::move(corrected_trajectory));
    auto relative_poses = trajectory.ToRelativePoses();
    slam::ApplyUniformNoise(relative_poses, options.rnoise_rotation, options.rnoise_translation);
    trajectory = slam::LinearContinuousTrajectory::FromRelativePoses(std::move(relative_poses));

    LOG(INFO) << "Saving the noisy trajectory with " << trajectory.Poses().size() << " poses at "
              << options.output_trajectory_path << std::endl;
    // Save the trajectory
    slam::SavePosesAsPLY(options.output_trajectory_path,
                         trajectory.Poses());
    return 0;
}

